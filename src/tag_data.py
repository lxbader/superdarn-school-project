from __future__ import unicode_literals

from datetime import datetime, timedelta
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.colorbar import Colorbar
import matplotlib.colors as colors
import matplotlib.gridspec as gridspec
import numpy as np
from pathlib import Path
from PyQt5 import QtCore, QtWidgets
import superdarn
import sys

progname = 'SuperDARN Tagger'
progversion = '0.1'

PATH_TO_DATA = Path.cwd() / '../sample_data'
# =============================================================================
# Play around with the DPI value to make plots have a good size and prevent
# them from getting smaller
# (if behaviour is weird in the initial window, it might still work in
# full screen mode)
# Recommended settings:
# 1920 x 1080 screen resolution: 150 DPI
# 1360 x 768 screen resolution: 100 DPI
# =============================================================================
DPI = 100

# =============================================================================
# comment the following line out of you want to directly
# open the sample file folder without having to navigate to it
# =============================================================================
PATH_TO_DATA = None


# velocity maximum for colorbars
MAX_VEL = 500
# lowest latitude to plot
LATMIN = 55
# number of bins in longitude/latitude for tagging boundaries
HBINS = 72
VBINS = 90-LATMIN

# =============================================================================
# Class keeping track of all backend data
# =============================================================================
class DataContainer(QtCore.QObject):
    updateSignal = QtCore.pyqtSignal() # update map
    newFileSignal = QtCore.pyqtSignal() # new file loaded
    fileErrorSignal = QtCore.pyqtSignal() # file couldn't be loaded
    updatePointsSignal = QtCore.pyqtSignal() # tagging points changed
    
    def __init__(self):
        QtCore.QObject.__init__(self)
        self.current_mapindex = None
        self.erroractive = False
        self.boundlist = ['H_EW','H_MAGN','V_1','V_2']
        
    def open_folder(self, folder):
        self.filelist = sorted(Path(folder).glob('*.cnvmap'))
        if len(self.filelist):
            self.get_data(0)
            return 0
        else:
            self.reset()
            return 1

    def get_data(self, fileindex, last=False):
        self.save_points()
        try:
            mapfile = self.filelist[fileindex]
            self.current_fileindex = fileindex
            darn_read = superdarn.DarnRead(mapfile)
            self.filedata = darn_read.read_map()
            
            self.starttimes = np.zeros((len(self.filedata)), dtype=datetime)
            for iii in range(len(self.filedata)):
                self.starttimes[iii] = datetime(self.filedata[iii]['start.year'].value,
                          self.filedata[iii]['start.month'].value,
                          self.filedata[iii]['start.day'].value,
                          self.filedata[iii]['start.hour'].value,
                          self.filedata[iii]['start.minute'].value,
                          int(self.filedata[iii]['start.second'].value))
        except:
            self.erroractive = True
            self.fileErrorSignal.emit()
        else:
            self.erroractive = False
            self.get_points()
            if last:
                self.get_map(len(self.filedata)-1)
            else:
                self.get_map(0)
            self.newFileSignal.emit()
    
    def get_map(self, index):
        # save points before switching map
        self.save_points()
        if self.erroractive:
            return
        self.current_mapindex = index
        self.mltav = self.filedata[index]['mlt.av'].value
        self.mlat = self.filedata[index]['vector.mlat'].value
        self.mlon = self.filedata[index]['vector.mlon'].value
        self.kvect = self.filedata[index]['vector.kvect'].value
        self.vel_med = self.filedata[index]['vector.vel.median'].value
        self.updateSignal.emit()
        
    def next_map(self):
        if self.current_mapindex is None:
            return
        if self.erroractive:
            return
        if self.current_mapindex < len(self.filedata)-1:
            self.get_map(self.current_mapindex+1)
        elif self.current_fileindex < len(self.filelist)-1:
            self.get_data(self.current_fileindex+1)
        else:
            self.save_points() # save points if no next map available
            
    def previous_map(self):
        if self.current_mapindex is None:
            return
        if self.erroractive:
            return
        if self.current_mapindex > 0:
            self.get_map(self.current_mapindex-1)
        elif self.current_fileindex > 0:
            self.get_data(self.current_fileindex-1, last=True)
        else:
            self.save_points() # save points if no previous map available
            
    def get_points(self):
        self.h_bins = np.linspace(0, 360, num=HBINS+1)
        self.h_centers = self.h_bins[:-1]+np.diff(self.h_bins)/2
        self.v_bins = np.linspace(LATMIN, 90, num=VBINS+1)
        self.v_centers = self.v_bins[:-1]+np.diff(self.v_bins)/2
        try:
            # load saved file
            thispath = self.filelist[self.current_fileindex]
            loadfile = thispath.parent.joinpath('{}_data.txt'.format(
                    thispath.stem))
            with open(loadfile, 'r') as infile:
                self.all_points = {}
                # number of maps
                infile.readline()
                self.all_points['NUMBER_OF_MAPS'] = np.array([
                        infile.readline().strip('\n')]).astype(np.float)
                infile.readline()
                # vectors giving the central lon (H boundaries) or lat (V boundaries)
                for iii in range(2):
                    name = infile.readline()[2:-1]
                    self.all_points[name] = np.loadtxt(infile, max_rows=1)
                    infile.readline()
                # actual boundary points as arrays
                for iii in range(4):
                    name = infile.readline()[2:-1]
                    self.all_points[name] = np.loadtxt(infile,
                              max_rows=int(self.all_points['NUMBER_OF_MAPS'][0]))
                    infile.readline()
            if name != 'V_2':
                raise Exception
        except:
            # create new array for saving points
            self.all_points = {}
            self.all_points['NUMBER_OF_MAPS'] = len(self.filedata)
            self.all_points['H_CENTERS_MAGLON'] = self.h_centers
            self.all_points['V_CENTERS_LAT'] = self.v_centers
            self.all_points['H_EW'] = np.full((len(self.filedata),
                           len(self.h_centers)), np.nan)
            self.all_points['H_MAGN'] = np.full((len(self.filedata),
                           len(self.h_centers)), np.nan)
            self.all_points['V_1'] = np.full((len(self.filedata),
                           len(self.v_centers)), np.nan)
            self.all_points['V_2'] = np.full((len(self.filedata),
                           len(self.v_centers)), np.nan)
            
    def save_points(self):
        if self.current_mapindex is None:
            return
        thispath = self.filelist[self.current_fileindex]
        savefile = thispath.parent.joinpath('{}_data.txt'.format(thispath.stem))
        # save dictionary in human readable text file
        with open(savefile, 'w') as outfile:
            for iii in [*self.all_points]:
                outfile.write('# {}\n'.format(iii))
                if iii in ['NUMBER_OF_MAPS', 'H_CENTERS_MAGLON', 'V_CENTERS_LAT']:
                    np.savetxt(outfile, np.array([self.all_points[iii]]), fmt='%0.2f')
                else:
                    np.savetxt(outfile, self.all_points[iii], fmt='%0.2f')
                outfile.write('\n'.format(iii))
            
    def add_point(self, lon, lat, key):
        if self.current_mapindex is None:
            return
        # reset points if previously marked as "nothing here"
        if np.all(self.all_points[key][self.current_mapindex, :] == -1):
            self.clear_points()
        # add single new point
        if key in ['H_EW','H_MAGN']:
            arglon = (np.digitize([lon], self.h_bins)-1)[0]
            self.all_points[key][self.current_mapindex, arglon] = lat
        elif key in ['V_1','V_2']:
            arglat = (np.digitize([lat], self.v_bins)-1)[0]
            self.all_points[key][self.current_mapindex, arglat] = lon
        self.updatePointsSignal.emit()
        
    def remove_point(self, lon, lat, key):
        if self.current_mapindex is None:
            return
        # remove single point
        if key in ['H_EW','H_MAGN']:
            arglon = (np.digitize([lon], self.h_bins)-1)[0]
            self.all_points[key][self.current_mapindex, arglon] = np.nan
        elif key in ['V_1','V_2']:
            arglat = (np.digitize([lat], self.v_bins)-1)[0]
            self.all_points[key][self.current_mapindex, arglat] = np.nan
            
        self.updatePointsSignal.emit()
        
    def clear_points(self):
        if self.current_mapindex is None:
            return
        # clear all points from this map
        for key in self.boundlist:
            self.all_points[key][self.current_mapindex, :] = np.nan
        self.updatePointsSignal.emit()
        
    def nothing_here(self):
        if self.current_mapindex is None:
            return
        for key in self.boundlist:
            self.all_points[key][self.current_mapindex, :] = -1
        self.updatePointsSignal.emit()
                
dc = DataContainer()
           
# =============================================================================
# Widget for keeping plots up to date
# CHANGE DPI VALUE FOR SCALING OF TEXT ETC (larger dpi = larger text)
# =============================================================================
class PlotWidget(FigureCanvas):
            
    def __init__(self, parent=None, width=20, height=6, dpi=DPI):
        # set up figure
        self.fig = Figure(figsize=(width, height), dpi=dpi, tight_layout=True)
        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)
        # set up gridspec with subplots
        gs = gridspec.GridSpec(3, 7,
                               width_ratios=(1.5, 0.15, 0.05, 0.35, 1, 0.05, 0.05),
                               wspace=0)
        self.map_axes = self.fig.add_subplot(gs[:,0], projection='polar')
        self.map_caxes = self.fig.add_subplot(gs[:,2])
        self.ew_axes = self.fig.add_subplot(gs[0,4])
        self.ew_caxes = self.fig.add_subplot(gs[0,6])
        self.ns_axes = self.fig.add_subplot(gs[1,4])
        self.ns_caxes = self.fig.add_subplot(gs[1,6])
        self.magn_axes = self.fig.add_subplot(gs[2,4])
        self.magn_caxes = self.fig.add_subplot(gs[2,6])
        
        self.initial_plot() 
        
    def update_figure(self):
        self.plot_figure()
        self.update_points()
        self.draw() 
        
    def update_points(self):
        # remove points
        try:
            for iii in range(len(self.points_quads)):
                self.points_quads[iii].remove()
        except:
            pass
        # remove text
        for ttt in self.texts:
            ttt.set_text('')
        # if no map selected
        if dc.current_mapindex is None:
            return
        # add text if marked as nothing here
        if np.all(dc.all_points['H_EW'][dc.current_mapindex,:]==-1):
            for ttt in self.texts:
                ttt.set_text('Marked as "nothing to see"\n click to add points')
        else:
            self.points_quads = []
            for ctr in range(2):
                tmpax = [self.ew_axes, self.magn_axes][ctr]
                tmpfc = ['gold', 'red'][ctr]
                tmplbl = ['H_EW','H_MAGN'][ctr]
                thisrow = dc.all_points[tmplbl][dc.current_mapindex,:]
                valid = np.where(np.isfinite(thisrow))[0]
                if len(valid):
                    tmp_quad = tmpax.scatter(dc.all_points['H_CENTERS_MAGLON'][valid],
                                             thisrow[valid],
                                             s=12,
                                             facecolor=tmpfc, edgecolor='k',
                                             zorder=20)
                    self.points_quads.append(tmp_quad)
        self.rescale()
        self.draw()

    def rescale(self):
        # rescale the radial axis of the polar map plot
        self.map_axes.set_theta_zero_location('S')
        self.map_axes.set_rmax(35)
        self.map_axes.set_rmin(0) 
        
    def initial_plot(self):
        self.plot_figure()
        # set up map plot
        self.map_quad.set_clim(50, MAX_VEL)
        cb = Colorbar(self.map_caxes, self.map_quad)
        cb.set_label('vel (m/s)', rotation=270, labelpad=10)
        self.map_axes.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
        self.map_axes.set_xticklabels(['00','06','12','18'])
        self.map_axes.set_yticks([10,20,30])
        self.map_axes.set_yticklabels([])
        self.map_axes.grid('on', color='k', linestyle='--', linewidth=1, zorder=0)

        # set up east-west and north-south plots
        axes = [self.ew_axes, self.ns_axes, self.magn_axes]
        caxes = [self.ew_caxes, self.ns_caxes, self.magn_caxes]
        quads = [self.ew_quad, self.ns_quad, self.magn_quad]
        labels = ['E $\\leftarrow$ v (m/s) $\\rightarrow$ W',
                  'N $\\leftarrow$ v (m/s) $\\rightarrow$ S',
                  'v (m/s)']
        self.ew_txt = None
        self.ns_txt = None
        self.magn_txt = None
        self.texts = [self.ew_txt, self.ns_txt, self.magn_txt]
        
        for ctr in range(3):
            tmpax = axes[ctr]
            # set up colorbars
            quads[ctr].set_clim(50 if ctr==2 else -1, MAX_VEL if ctr==2 else 1)
            cb = Colorbar(caxes[ctr], quads[ctr])
            cb.set_label(labels[ctr], rotation=270, labelpad=12 if ctr==2 else 25)
            if ctr<2:
                cb.set_ticks([])
            else:
                cb.ax.tick_params(labelsize=8, rotation=315)
            # set up background, labels and ticks
            tmpax.set_facecolor('grey')
            tmpax.set_xlim([0,360])
            tmpax.set_xticks(np.arange(0,361,90))
            tmpax.set_xticklabels(['{:02d}'.format(iii) for iii in np.arange(0,25,6)])
            tmpax.set_ylim([LATMIN, 90])
            tmpax.set_yticks(np.arange(60, 91, 10))
            if ctr==2: tmpax.set_xlabel('MLT (h)')
            tmpax.set_ylabel('MLat (deg)')
            tmpax.grid('on', color='k', linestyle='--', linewidth=0.5, zorder=5)
        
            self.texts[ctr] = tmpax.text(0.5, 0.95, '',
                      transform=tmpax.transAxes,
                      color='k', fontsize=9,
                      ha='center', va='top')
        
    def plot_figure(self):
        for tmpax in [self.map_axes, self.ew_axes, self.ns_axes, self.magn_axes]:
            for artist in tmpax.lines + tmpax.collections:
                artist.remove()
        # make empty plot for startup
        if (dc.current_mapindex is None) or (len(dc.mlat)==0):
            self.map_quad = self.map_axes.quiver([100], [100], [100], [100], [100], cmap='viridis')
            a = np.array([1,2])
            b = np.full((2,2), np.nan)
            self.ew_quad = self.ew_axes.pcolormesh(a, a, b, cmap='seismic')
            self.ns_quad = self.ns_axes.pcolormesh(a, a, b, cmap='seismic')
            self.magn_quad = self.magn_axes.pcolormesh(a, a, b, cmap='viridis')
        else:
            #---- Map plot
            # location of the measurement in polar coordinates
            r = 90-dc.mlat
            t = (np.radians(dc.mlon)+dc.mltav/12*np.pi) % (2*np.pi)
            # direction of the arrows, need to calculate in x-y coords
            dr = 1
            dt = np.radians((90-dc.kvect+dc.mltav/12*180+ dc.mlon) % 360)
            u = dr * np.cos(dt)
            v = dr * np.sin(dt)
            # plot (better arrowstyles would be good)
            self.map_quad = self.map_axes.quiver(t, r, u, v, dc.vel_med, cmap='viridis',
                                                 norm=colors.Normalize(vmin=50, vmax=MAX_VEL))
            
            #---- east-west and north-south plots
            # measurement location
            lat = dc.mlat
            lon = (dc.mlon+dc.mltav/12*180) % 360
            # split velocities in east/west and north/south
            xvel = dc.vel_med*np.sin(np.radians(dc.kvect))
            yvel = dc.vel_med*np.cos(np.radians(dc.kvect))
            # make latitude bins
            lats = np.linspace(55, 90, num=36)
            arglat = np.digitize(lat, lats)-1
            for iii in range(len(lats-1)):
                # for each latitude bin, check data availability
                selec = np.where(arglat==iii)[0]
                if not len(selec):
                    continue
                # make bins in the longitudinal direction (Ruohoniemi & Baker 1998)
                nbins = np.round(360*np.sin(np.radians(90-lat[selec][0])))
                lons = np.linspace(0, 360, num=int(nbins+1))
                # shift bins if necessary to fit the original binning
                minpoint = np.min(lon[selec])
                loncenters = lons[:-1]+np.diff(lons)/2
                arg = np.digitize(minpoint,loncenters)-1
                offs = minpoint-loncenters[arg]
                lons = np.append([0], (lons+offs)%360)
                lons[-1] = 360
                arglon = np.digitize(lon[selec], lons)-1
                # average velocity measurements in these bins
                xvels = np.full((len(lons)-1), np.nan)
                yvels = np.full((len(lons)-1), np.nan)
                totvels = np.full((len(lons)-1), np.nan)
                for jjj in np.unique(arglon):
                    tmp = selec[np.where(arglon==jjj)[0]]
                    totvel = np.sqrt(np.mean(xvel[tmp])**2 + np.mean(yvel[tmp])**2)
                    xvels[jjj] = np.mean(xvel[tmp])/totvel
                    yvels[jjj] = np.mean(yvel[tmp])/totvel
                    totvels[jjj] = np.mean(dc.vel_med[tmp])
                    if jjj in [0, len(lons)-2]:
                        tmp = len(lons)-2 if jjj==0 else 0
                        xvels[tmp] = xvels[jjj]
                        yvels[tmp] = yvels[jjj]
                        totvels[tmp] = totvels[jjj]
                        
                # plot
                self.ew_axes.pcolormesh(lons, lats[iii:iii+2], np.array([xvels]), cmap='seismic', 
                                        norm=colors.Normalize(vmin=-1, vmax=1))
                self.ns_axes.pcolormesh(lons, lats[iii:iii+2], np.array([yvels]), cmap='seismic',
                                        norm=colors.Normalize(vmin=-1, vmax=1))
                self.magn_axes.pcolormesh(lons, lats[iii:iii+2], np.array([totvels]), cmap='viridis',
                                          norm=colors.Normalize(vmin=50, vmax=MAX_VEL))
                
                # frame areas with velocity below 50
                cond = totvels>50
                xvels[cond] = np.nan
                self.ew_axes.pcolor(lons, lats[iii:iii+2], np.array([xvels]), cmap='seismic', 
                                        norm=colors.Normalize(vmin=-1, vmax=1), linewidths=0.7, ec='k', zorder=10)
                yvels[cond] = np.nan
                self.ns_axes.pcolor(lons, lats[iii:iii+2], np.array([yvels]), cmap='seismic',
                                        norm=colors.Normalize(vmin=-1, vmax=1), linewidths=0.7, ec='k', zorder=10)
                totvels[cond] = np.nan
                self.magn_axes.pcolor(lons, lats[iii:iii+2], np.array([totvels]), cmap='viridis',
                                          norm=colors.Normalize(vmin=50, vmax=MAX_VEL), linewidths=0.7, ec='k', zorder=10)

            # turn on grid
            for tmpax in [self.ew_axes, self.ns_axes, self.magn_axes]:
                tmpax.grid('on', color='k', linestyle='--', linewidth=0.5, zorder=5)
                    
        # rescale polar plot to right radius
        self.rescale()

    def clear_data(self):
        for tmpax in [self.map_axes, self.ew_axes, self.ns_axes, self.magn_axes]:
            for artist in tmpax.lines + tmpax.collections:
                artist.remove()
        self.draw()
        
    def on_click(self, event):
        valid_axes = [self.ew_axes, self.magn_axes]
        if event.inaxes == self.ew_axes:
            key = 'H_EW'
        elif event.inaxes == self.magn_axes:
            key = 'H_MAGN'
        if event.button==1 and event.inaxes in valid_axes:
            dc.add_point(event.xdata, event.ydata, key)
        if event.button==3 and event.inaxes in valid_axes:
            dc.remove_point(event.xdata, event.ydata, key)
#            self.draw_dot(event.xdata, event.ydata)
            
        
# =============================================================================
# Main window, organizing layout and functionality     
# =============================================================================
class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle('{}, Version {}'.format(progname,progversion))
        
        #----
        # menu bar on top
        self.file_menu = QtWidgets.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)
                    
        self.help_menu = QtWidgets.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)
        self.help_menu.addAction('&Instructions', self.instructions)
        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtWidgets.QWidget(self)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)
        
        box = QtWidgets.QGridLayout(self.main_widget)
        
        #----
        # main plot
        self.pw = PlotWidget(self.main_widget)
        box.addWidget(self.pw,1,1)
                
        #----
        # menu for file and map browsing
        
        # open folder button
        v = QtWidgets.QVBoxLayout()
        self.open_button = QtWidgets.QPushButton('Open folder...', self)
        self.open_button.clicked.connect(self.on_open_button_clicked)
        v.addWidget(self.open_button)
        
        # list of files in folder
        tmp = QtWidgets.QLabel('<b>Available files</b>', self)
        v.addWidget(tmp)
        self.file_list = QtWidgets.QListWidget(self)
        self.file_list.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)        
        self.file_list.currentRowChanged.connect(self.on_filenumber_changed)
        v.addWidget(self.file_list)
        
        # timestamp of current map
        tmp = QtWidgets.QLabel('<b>Timestamp</b>', self)
        v.addWidget(tmp)
        self.timestamp_label = QtWidgets.QLabel('', self)
        v.addWidget(self.timestamp_label)
        
        # which map in this file
        sbox = QtWidgets.QGridLayout()        
        tmp = QtWidgets.QLabel('<b>Select map</b>', self)
        sbox.addWidget(tmp,1,1)
        self.mapnumber_box = QtWidgets.QComboBox(self)
        self.mapnumber_box.currentIndexChanged.connect(self.on_mapnumber_changed)
        sbox.addWidget(self.mapnumber_box,2,1)
        tmp = QtWidgets.QLabel('<b>Available maps</b>', self)
        sbox.addWidget(tmp,1,2)
        self.mapnumber_label = QtWidgets.QLabel('', self)
        sbox.addWidget(self.mapnumber_label,2,2)
        v.addLayout(sbox)
        
        # previous / next buttons
        h1 = QtWidgets.QHBoxLayout()        
        self.prev_button = QtWidgets.QPushButton('< Previous map', self)
        self.prev_button.clicked.connect(self.on_prev_button_clicked)
        h1.addWidget(self.prev_button)                
        self.next_button = QtWidgets.QPushButton('Next map >', self)
        self.next_button.clicked.connect(self.on_next_button_clicked)
        h1.addWidget(self.next_button)
        v.addLayout(h1)
        box.addLayout(v,1,2)
        
        v.addWidget(self.HLine())
        
        #----
        # menu for handling datapoints
        self.nothing_button = QtWidgets.QPushButton('Nothing to see here', self)
        self.nothing_button.clicked.connect(self.on_nothing_button_clicked)
        v.addWidget(self.nothing_button)
        
        self.clear_button = QtWidgets.QPushButton('Clear datapoints', self)
        self.clear_button.clicked.connect(self.on_clear_button_clicked)
        v.addWidget(self.clear_button)
        
        self.save_button = QtWidgets.QPushButton('Save datapoints', self)
        self.save_button.clicked.connect(self.on_save_button_clicked)
        v.addWidget(self.save_button)
        
        #----
        dc.updateSignal.connect(self.update)
        dc.newFileSignal.connect(self.file_update)
        dc.fileErrorSignal.connect(self.file_error)
        dc.updatePointsSignal.connect(self.pw.update_points)
        self.statusBar().showMessage("Ready", 2000)
        
    def HLine(self):
        toto = QtWidgets.QFrame()
        toto.setFrameShape(QtWidgets.QFrame.HLine)
        toto.setFrameShadow(QtWidgets.QFrame.Sunken)
        return toto

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()
        
    def instructions(self):
        QtWidgets.QMessageBox.about(self, "Instructions", """Click to add points and right-click to remove them""")

    def about(self):
        QtWidgets.QMessageBox.about(self, "About", """Created by Alexander Bader,
Lancaster University, October 2019.
Contact: a.bader@lancaster.ac.uk""")
        
    def update(self):
        self.pw.update_figure()
        fmt = '%Y-%m-%d %H:%M:%S'
        fmt2 = '%H:%M:%S'
        tmp1 = datetime.strftime(dc.starttimes[dc.current_mapindex], fmt)
        tmp2 = datetime.strftime(dc.starttimes[dc.current_mapindex]+timedelta(minutes=2), fmt2)
        self.timestamp_label.setText('{}-{}'.format(tmp1, tmp2))
        self.mapnumber_label.setText('{}'.format(len(dc.filedata)))
        self.mapnumber_box.setCurrentIndex(dc.current_mapindex)
        
    def file_update(self):
        self.file_list.setCurrentRow(dc.current_fileindex)
        self.fill_mapnumberlist()
        
    def file_error(self):
        self.pw.clear_data()
        self.timestamp_label.setText('n/a')
        self.mapnumber_label.setText('n/a')
        self.mapnumber_box.clear()
        self.file_list.setCurrentRow(dc.current_fileindex)
        QtWidgets.QMessageBox.warning(self, 'Warning',
                                      '''File could not be opened''')
        
    def on_open_button_clicked(self):
        if PATH_TO_DATA is None:
            dialog = QtWidgets.QFileDialog(self, 'Open folder', 'C:\\', 'MAP files (*.cnvmap)')
            dialog.setFileMode(QtWidgets.QFileDialog.DirectoryOnly)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                folder = dialog.selectedFiles()[0]
                err = dc.open_folder(folder)
                if err:
                    QtWidgets.QMessageBox.warning(self, 'Warning',
                                                  '''No MAP files in selected folder''')
        # for testing
        else:
            dc.open_folder(PATH_TO_DATA)
        self.update_file_list()
        
    def update_file_list(self):
        # disconnect signal while filling list to avoid multiple triggers
        self.file_list.currentRowChanged.disconnect(self.on_filenumber_changed)
        self.file_list.clear()
        tmp = dc.filelist
        if len(tmp):
            for iii in range(len(tmp)):
                text = tmp[iii].parts[-1]
                self.file_list.addItem(text)
            self.file_list.setCurrentRow(dc.current_fileindex)
        self.file_list.currentRowChanged.connect(self.on_filenumber_changed)
            
    def on_filenumber_changed(self):
        if self.file_list.count() and (self.file_list.currentRow() != dc.current_fileindex):
            dc.get_data(self.file_list.currentRow())
                
    def fill_mapnumberlist(self):
        # disconnect signal while filling list to avoid multiple triggers
        self.mapnumber_box.currentIndexChanged.disconnect(self.on_mapnumber_changed)
        self.mapnumber_box.clear()
        for iii in range(len(dc.filedata)):
            self.mapnumber_box.addItem('{}'.format(iii+1))
        self.mapnumber_box.setCurrentIndex(dc.current_mapindex)
        self.mapnumber_box.currentIndexChanged.connect(self.on_mapnumber_changed)
                
    def on_mapnumber_changed(self):
        tmp = self.mapnumber_box.currentIndex()
        if tmp != dc.current_mapindex and tmp>=0:
            dc.get_map(tmp)
    
    def on_prev_button_clicked(self):
        dc.previous_map()
        
    def on_next_button_clicked(self):
        dc.next_map()
        
    def on_nothing_button_clicked(self):
        dc.nothing_here()
        
    def on_clear_button_clicked(self):
        dc.clear_points()
    
    def on_save_button_clicked(self):
        dc.save_points()
        
    def keyPressEvent(self, event):
        pass

qApp = QtWidgets.QApplication(sys.argv)

aw = ApplicationWindow()
aw.setWindowTitle("%s, Version %s" % (progname,progversion))
aw.show()
sys.exit(qApp.exec_())
#qApp.exec_()