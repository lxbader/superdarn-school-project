from datetime import datetime
import glob
import matplotlib.pyplot as plt
import numpy as np
import superdarn

PATH_TO_DATA = 'E:/GIT/superdarn-school-project/sample_data'
#PATH_TO_DATA = 'D:/PhD/GIT/superdarn-school-project/sample_data'
mapfile = glob.glob('{}/*.cnvmap'.format(PATH_TO_DATA))[0]

darn_read = superdarn.DarnRead(mapfile)
mapdata = darn_read.read_map()

def get_times(md):
    starttimes = np.zeros((len(md)), dtype=datetime)
    for iii in range(len(md)):
        starttimes[iii] = datetime(md[iii]['start.year'].value,
                  md[iii]['start.month'].value,
                  md[iii]['start.day'].value,
                  md[iii]['start.hour'].value,
                  md[iii]['start.minute'].value,
                  int(md[iii]['start.second'].value))
    return starttimes

starttimes = get_times(mapdata)

# parameter names
[*mapdata[0]]


#%%
cnt = 0

mltav = mapdata[cnt]['mlt.av'].value
mlat = mapdata[cnt]['vector.mlat'].value
mlon = mapdata[cnt]['vector.mlon'].value
kvect = mapdata[cnt]['vector.kvect'].value
vel_med = mapdata[cnt]['vector.vel.median'].value

#%%
# location of the measurement in polar coordinates
r = 90-mlat
t = (np.radians(mlon)+mltav/12*np.pi) % (2*np.pi)

# direction of the arrows, need to calculate in x-y coords
dr = 1
dt = np.radians((90-kvect+mltav/12*180+ mlon) % 360)
u = dr * np.cos(dt)
v = dr * np.sin(dt)

fig = plt.figure()
fig.set_size_inches(10,10)
ax = plt.subplot(projection='polar')
quad = ax.quiver(t, r, u, v, vel_med, cmap='viridis')
plt.colorbar(quad, ax=ax)
ax.set_theta_zero_location('S')
plt.show()
plt.close()



#%%
# location of the measurement in lon-lat
r = mlat
t = (np.radians(mlon)+mltav/12*np.pi) % (2*np.pi)
# arrow directions
dr = 1
dt = np.radians(kvect)
u = dr*np.sin(dt)
v = dr*np.cos(dt)
# plot
fig = plt.figure()
fig.set_size_inches(10,5)
ax = plt.subplot()
ax.quiver(t, r, u, v, vel_med, cmap='viridis')
plt.show()
plt.close()



#%%

fig = plt.figure()
fig.set_size_inches(10,10)
ax1 = plt.subplot(211)
ax2 = plt.subplot(212)

# measurement location
lat = mlat
lon = (mlon+mltav/12*180) % 360

xvel = vel_med*np.sin(np.radians(kvect))
yvel = vel_med*np.cos(np.radians(kvect))

# make bins
lats = np.linspace(55, 90, num=36)
arglat = np.digitize(lat, lats)-1

for iii in range(len(lats-1)):
    selec = np.where(arglat==iii)[0]
    if not len(selec):
        continue
    nbins = np.round(360*np.sin(np.radians(90-lat[selec][0])))
    
    lons = np.linspace(0, 360, num=int(nbins+1))
    arglon = np.digitize(lon[selec], lons)-1
    
    xvels = np.full((len(lons)-1), np.nan)
    yvels = np.full((len(lons)-1), np.nan)
    for jjj in np.unique(arglon):
        tmp = selec[np.where(arglon==jjj)[0]]
        xvels[jjj] = np.mean(xvel[tmp])
        yvels[jjj] = np.mean(yvel[tmp])
    
    quad1 = ax1.pcolormesh(lons, lats[iii:iii+2], np.array([xvels]), cmap='cividis')
    #quad1 = ax1.tricontourf(lon, lat, xvel, cmap='cividis')
    quad2 = ax2.pcolormesh(lons, lats[iii:iii+2], np.array([yvels]), cmap='cividis')

#lons = np.linspace(0, 360, num=91)
#arglon = np.digitize(lon, lons)-1
#
#
#
#
#xvels = np.full((len(lons)-1, len(lats)-1), np.nan)
#yvels = np.full((len(lons)-1, len(lats)-1), np.nan)
#
#for iii in range(xvels.shape[0]):
#    for jjj in range(xvels.shape[1]):
#        tmp = np.where((arglon==iii) & (arglat==jjj))[0]
#        xvels[iii,jjj] = np.mean(xvel[tmp])
#        yvels[iii,jjj] = np.mean(yvel[tmp])

plt.colorbar(quad, ax=ax1)
plt.colorbar(quad, ax=ax2)
plt.show()
#plt.close()
