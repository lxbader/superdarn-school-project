# SuperDARN Tagger

## Requirements
Should run on a clean Anaconda install. 
Tested on Python 3.7 with Matplotlib, Numpy and PyQt5.


## Installation and startup
Copy this code (all files) onto the machine you want to use it on.

Open a command line, navigate to the folder the code is located in using for example
`cd E:\GIT\superdarn-school-project\src` (you may have to switch to the right drive first, using e.g. `E:`)
and run the tool using
`python tag_data.py`


## Usage
Click `Open folder...` and navigate to the folder your data is located in. Don't worry if you can't see any files, they will show up in the tool once the folder selection window is closed.

Navigate between the different SuperDARN files by clicking them in the list. Each file typically contains several maps, between which you can navigate below the file list.

Set datapoints by left clicking on the top and bottom panels, remove them by right clicking on them. You can clear all datapoints by clicking the appropriate button on the bottom right. 

If you cannot identify any interesting boundaries, click `Nothing to see here` so know which data has been looked at. 

Your datapoints are saved automatically when switching between maps or files using the in-tool navigation options. However, click on `Save datapoints` before closing the program, otherwise your currently shown map will not be saved.


## Data storage
Your marked datapoints are stored in .txt files in the same folder where the original data is located in. Make sure to copy them to a safe location so they don't get lost (desktops of work/school network machines may be wiped after logout and things like that). 

Data included
* NUMBER_OF_MAPS: number of maps included in this file
* H_CENTERS_MAGLON: datapoint centers of "horizontal" boundaries (boundaries roughly in east-west direction) in degrees magnetic longitude
* V_CENTERS_LAT: datapoint centers of "vertical" boundaries (boundaries roughly in north-south direction) in degrees magnetic latitude (90 deg = north pole)
* H_EW: for each map, datapoint centers of "horizontal" boundaries marked in east-west flow panel (top) in degrees magnetic latitude
* H_MAGN: for each map, datapoint centers of "horizontal" boundaries marked in flow magnitude panel (bottom) in degrees magnetic latitude
* V_1, V_2: unused, for vertical boundaries